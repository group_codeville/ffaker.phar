<?php
	
	/*
		FinFakerBase
		Base class for ffaker classes

		This file is part of ffaker.phar project
	*/
	class FinFakerBase {
		
		const FFVersion = '0.0.2';

		protected $_db = null;
		protected $_dbConf;

		public function __construct($db) {
			$this->_dbConf = $db;

			$connString = $db['driver'] . ':host=' . $db['host'] . ';port=' . $db['port'] . ';dbname=' . $db['db'];
			$this->_db = new PDO($connString, $db['user'], $db['pass']);
		}

		public static function version() {
			return self::FFVersion;
		}

		/*
			TODO:
				add default sizes by type
		*/
		public static function defaultSize($type) {
			return 10;
		}

		protected function getType($val) {
			$type = explode('(', $val)[0];

			if($type == 'varchar' || $type == 'text')
				return 'char';
			return $type;
		}

		protected function getSize($val) {
			$result = 11;
			
			if(preg_match("/\((\d+)\)/", $val, $matches)) {
				$result = $matches[1];
			}
			return $result;
		}

		protected function getTables() {
			$q = $this->_db->query("SHOW TABLES");
			return $q->fetchAll(PDO::FETCH_COLUMN);
		}

		protected function tableFields($table) {
			$q = $this->_db->query("SHOW COLUMNS FROM " . $table);
			return $q->fetchAll();
		}

		protected function getPrimaryKey($table) {
			$pk = $this->_db->prepare("SHOW KEYS FROM $table WHERE Key_name = 'PRIMARY'");
			$pk->execute();
			$meta = $pk->fetch();

			if(isset($meta['Column_name']))
				return $meta['Column_name'];

			return null;
		}

		protected function tableForeignKeys($table) {
			$sql = "SELECT *
					FROM
					  information_schema.KEY_COLUMN_USAGE
					WHERE
					  TABLE_NAME = :table
					  AND 
					  TABLE_SCHEMA = :db
					  AND
					  CONSTRAINT_NAME != 'PRIMARY'";

			$c = $this->_db->prepare($sql);
			$c->execute(array(':db' => $this->_dbConf['db'], ':table' => $table));
			
			return $c->fetchAll();
		}

		protected function tableIndexes($table) {
			$sql = "SELECT * 
					FROM 
					  information_schema.statistics 
					WHERE 
					  table_schema = :db 
					  AND 
					  INDEX_NAME != 'PRIMARY'";

			$c = $this->_db->prepare($sql);
			$c->execute(array(':db' => $this->_dbConf['db']));

			return $c->fetchAll();
		}
	}