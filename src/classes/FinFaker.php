<?php

	/*
		FinFaker
		This class fill database with random data

		2016 @ikenfin
	*/

	require_once('FinFakerBase.php');

	class FinFaker extends FinFakerBase {

		protected $struct = [];
		protected $binded = [];

		protected $chars = [
			'q','w','e','r','t','y','u','i','o','p','a',
			's','d','f','g','h','j','k','l','z','x','c',
			'v','b','n','m',' '
		];

		protected $__supportedCalculateOperations = ['+', '-', '/', '*'];

		/* data generators */
		public function generateInt($max) {
			return rand(0, $max);
		}

		public function generateDouble($size) {
			list($max, $after) = explode(",", $size);
			return rand(0, $max) / pow(10, $after);
		}

		public function generateDatetime() {
			$rand_timestamp = $this->generateInt(time() - 86400);
			return date('Y-m-d H:i:s', $rand_timestamp);
		}

		public function generateChar($size) {
			$result = '';
			for($i = 0; $i < $size; $i++) {
				$result .= $this->chars[$this->generateInt(count($this->chars) - 1)];
			}
			return $result;
		}

		public function randomData($type, $size) {
			$method = 'generate' . ucfirst($type);
			if(method_exists($this, $method)) {
				return $this->{$method}($size);
			}
		}

		/*
			Находит рандомную строку в таблице $table
			Если $null == true, перед запросом выполняется возврат null если random вернул TRUE
		*/
		public function findRandom($table, $null = false) {
			if($null) {
				$returnNull = rand(0, 1);
				
				if($returnNull)
					return null;
			}

			$finder = $this->_db->prepare("SELECT * FROM  $table ORDER BY RAND() LIMIT 1");
			$finder->execute();
			$related = $finder->fetch(PDO::FETCH_ASSOC);

			return $related;
		}

		/*
			Заполняет данными из связанной таблицы
		*/
		public function relatedValue($name, $relation, $null=false) {
			list($table, $field) = explode('.', $relation);
			
			if($table == 'self')
				$table = $this->struct['__table__'];

			if(!isset($this->binded['__related__']))
				$this->binded['__related__'] = [];

			if(!array_key_exists($name, $this->binded['__related__']))
				$this->binded['__related__'][$name] = $this->findRandom($table, $null);

			$value = null;

			if($field == 'pk') {
				$field = $this->getPrimaryKey($table);
			}

			if(isset($this->binded['__related__'][$name][$field])) {
				$value = $this->binded['__related__'][$name][$field];
			}
			else {
				if(!$null)
					throw new Exception("We cannot find anything to relate and field $name cannot be null!");	
			}

			return $value;
		}

		protected function getValue($field_string, $null = false) {
			if(is_numeric($field_string))
				return $field_string;

			if(strpos($field_string, '.')) {
				list($key, $value) = explode('.', $field_string);

				$struct_item = $this->struct[$key];

				if($struct_item['related']) {
					if(isset($this->binded['__related__'][$key][$value])) {
						return $this->binded['__related__'][$key][$value];
					}
				}
				else {
					return $this->binded[$value];
				}
			}
			elseif(array_key_exists($field_string, $this->binded)) {
				return $this->binded[$field_string];
			}

			return null;
		}

		public function calculatedValue($name, $expression, $null, $default = null) {
			list($a_str, $expr, $b_str) = explode(' ', $expression);

			if(!in_array($expr, $this->__supportedCalculateOperations))
				throw new Exception("{ $expr } is not supported in calculatedValue! Supported operators: [" . implode(',', self::$__supportedCalculateOperations) . "]", 1);
				
			$a_value = $this->getValue($a_str, $null);
			$b_value = $this->getValue($b_str, $null);
			
			if($a_value == null || $b_value == null) {
				if($default !== null)
					return $default;
				if($null)
					return null;

				throw new Exception("Cannot calculate expression: { $a_value $expr $b_value }");
				
			}

			$val = eval('return ' . $a_value . $expr . $b_value . ';');

			if($val == null) {
				$val = $default;
			}

			if($val == null) {
				if(!$null)
					throw new Exception("Cannot calculate {$expression} because its given NULL and $name cannot be null!");
			}

			return $val;
		}

		public static function getArrayValue($array, $key, $default) {
			if(array_key_exists($key, $array))
				return $array[$key];
			return $default;
		}

		public function recognize($field, $struct_item) {
			$type = $struct_item[0];
			$size = self::getArrayValue($struct_item, 1, self::defaultSize($type));
			$auto = self::getArrayValue($struct_item, 'auto', false);
			$null = self::getArrayValue($struct_item, 'null', false);
			$value= self::getArrayValue($struct_item, 'value', null);
			$default = self::getArrayValue($struct_item, 'default', null);
			$related = self::getArrayValue($struct_item, 'related', null);

			if($auto == true)
				return;

			if($related) {
				$this->binded[$field] = $this->relatedValue($field, $related, $null);
				return;
			}
			if($value) {
				$this->binded[$field] = $this->calculatedValue($field, $value, $null, $default);
				return;
			}

			$this->binded[$field] = $this->randomData($type, $size);
		}

		public function run($count = 0, $struct = [], $chars = [], $progressCallback = null) {
			$this->struct = $struct;

			if(count($chars) > 0)
				$this->chars = $chars;

			if(empty($struct))
				throw new Exception("Structure is empty!");	

			$table = null;

			for($i = 0; $i < $count; $i++) {
				$this->binded = [];

				foreach($struct as $key => $struct_item) {
					if($key == '__table__') {
						continue;
					}
					$this->recognize($key, $struct_item);
				}

				$result = $this->save($this->binded);

				if(is_callable($progressCallback))
					call_user_func($progressCallback, $i+1, $count, $result);
			}
		}

		public function save($values) {
			$keys = array_keys($this->binded);
			// убираем related из набора ключей
			if(($idx = array_search('__related__', $keys)) !== false) {
				unset($keys[$idx]);
			}

			$keys_str = implode(',', array_values($keys));
			$binders = implode(',', array_map(function($v) {
				return ':' . $v;
			}, array_values($keys)));

			$insert_string = "INSERT INTO " . $this->struct['__table__'] . " (" . $keys_str . ") VALUES (" . $binders . ")";

			$_q = $this->_db->prepare($insert_string);

			foreach($keys as $key) {
				$_q->bindParam(':' . $key, $this->binded[$key]);
			}

			if(!$_q->execute()) {
				var_dump($_q->errorInfo());
				return false;
			}
			return true;
		}

	}