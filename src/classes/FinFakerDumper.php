<?php
	
	require_once('FinFakerBase.php');

	/*
		FinFakerDumper
		This class dumps structures from database

		This file is part of ffaker.phar project
	*/
	class FinFakerDumper extends FinFakerBase {

		const FFVersion = '0.0.4';

		protected $_exportTables = false;
		protected $_exportTablesList = [];
		
		protected $outFormat = 'PHP';

		public function __construct($db, array $exportTables = []) {
			$this->_exportTablesList = $exportTables;

			if(count($this->_exportTablesList) > 0)
				$this->_exportTables = true;

			parent::__construct($db);
		}

		public function dump($outfile, $include_db_config = false, $outFormat = null) {
			$struct = $this->dumpInternal();
			
			if($include_db_config) {
				$struct['__db_config__'] = $this->_dbConf;
			}

			if($outFormat != null) {
				$this->outFormat = $outFormat;
			}

			$this->generateFile($outfile, $struct);
		}

		protected function dumpInternal() {
			$tables = $this->getTables();
			$result = [];

			foreach($tables as $table) {
				if($this->_exportTables) {
					if(!in_array($table, $this->_exportTablesList)) {
						continue;
					}
				}

				$struct = [
					'__table__' => $table
				];

				$pk = $this->getPrimaryKey($table);
				$fields = $this->tableFields($table);
				$fks = $this->tableForeignKeys($table);
				
				foreach($fields as $data) {
					$field = [];

					if($data['Field'] == $pk) {
						$struct['pk'] = [$data['Field'], $this->getType($data['Type']), 'auto' => true];
						continue;
					}
					
					$field[0] = $this->getType($data['Type']);
					$field[1] = $this->getSize($data['Type']);

					if($fks != null && count($fks) > 0) {
						foreach($fks as $fk) {
							if($data['Field'] == $fk['COLUMN_NAME']) {
								$field['related'] = $fk['REFERENCED_TABLE_NAME'] . '.' . $fk['REFERENCED_COLUMN_NAME'];
							}
						}
					}

					if($data["Null"] == "YES")
						$field['null'] = true;

					$struct[$data['Field']] = $field;
 				}

 				$result[] = $struct;
			}

			return $result;
		}

		public function generateFile($file, $struct) {
			$file = fopen($file, 'w');
			$outMethod = 'generate' . $this->outFormat;
			
			if(method_exists($this, $outMethod))
				return call_user_func_array([$this, $outMethod], [$file, $struct]);
			else $this->generatePHP($file);

			fclose($file);
		}

		public function generatePHP($file, $struct) {
			fwrite($file, '<?php' . "\n");
			fwrite($file, 'return ');
			fwrite($file, var_export($struct, true));
			fwrite($file, "; \n");
		}

		public function generateJSON($file, $struct) {
			fwrite($file, json_encode($struct));
		}

		public function generateSerialized($file, $struct) {
			fwrite($file, serialize($struct));
		}

	}