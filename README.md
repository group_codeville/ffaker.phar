# ffaker

PHP script to generate fake data in db, with specific rules.

## Installation

ffaker is cmd wrapper for FinFaker php class, so you can use FinFaker separately, or you can compile ffaker.phar.

Compile ffaker.phar:

```shell
cd <ffaker_dir>
make # make will generate ffaker.phar in 'build' dir from now you can use it by ./build/ffaker.phar
sudo make install # this command will copy builded ffaker.phar to /usr/local/bin, so it will be accesible globally
```

## Using

ffaker requires two files:
	
1) database connection file (`-d key`)
	in this file you configure db connection

	example:

```php
<?php
	return[
		'host' => 'localhost',
		'port' => '3306',
		'db' => 'classified',
		'user' => 'root',
		'pass' => ''
	];
```

2) schema file (`-c key`)
	in this file you configure fields for table

	example:

```php
<?php
	return [
		'__table__' => 'address_object',
		'pk' => ['id_address_object', 'auto' => true],
		'name' => ['char', '240'],
		'coords_longitude' => ['double', '17,14', 'null' => true],
		'coords_latitude' => ['double', '17,14', 'null' => true],
		'type_info' => ['char', '36'],
		'type' => ['int', 11],
		'parent_id' => ['int', '11', 'related' => 'self.pk', 'null' => true],
		'level' => ['int', 11, 'value' => 'parent_id.level + 1', 'null' => true, 'default' => 0]
	];
```

For more info see help screen (`-h key`)