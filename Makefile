# This file is part of ffaker.phar project

INSTALL_DIR=/usr/local/bin

.SILENT:

program: build_all
	echo "Done. Run make install as root to install phar into your system"

install:
	cp build/*.phar $(INSTALL_DIR)

uninstall:
	rm $(INSTALL_DIR)/ffaker.phar
	rm $(INSTALL_DIR)/ffaker-dump.phar

clean:
	rm -rf pre_build
	rm -rf build/*

build_ffaker: prepare_php
	php build.php ffaker
	chmod +x build/ffaker.phar

build_ffaker_dumper: prepare_php
	php build.php ffaker-dump
	chmod +x build/ffaker-dump.phar

build_all: prepare_php
	php build.php all
	chmod +x build/*.phar

prepare_php: prepare_build
	echo "Prepare PHP scripts . . ."
	for file in $(shell find `pwd`/pre_build | grep .php\$$) ; do \
		`cat $$file | sed -e /^#\!.usr.*php\$$/d | tee $$file > /dev/null` ; \
	done

prepare_build:
	echo "Creating pre_build directory for additional processing . . ."
	rm -rf pre_build
	mkdir pre_build
	cp -R src/* pre_build/